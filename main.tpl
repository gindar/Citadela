<!doctype html>
<title>Citadela WebServer - %(title)s</title>
<style>
    body, html {font-family: monospace;font-size:14px;}
    h1 {font-size: 20px;}
    .menu{float:right;}
    .footer{color:#666; font-size: 11px;}
    .status_war, .status_accepting, .status_results {font-weight: bold;font-size: 14px;}
    .status_accepting {color:#060;}
    .status_war {color:#600;}
    .status_results {color:#006;}
    span.ok{color:#060;font-weight: bold;}
    span.err{color:#600;font-weight: bold;}
    table tr th {text-align: left;}
</style>
<div class="menu">
    [<a href="/status">Status</a>]
    [<a href="/results">Results</a>]
    [<a href="/results/detail">Detailed results</a>]
</div>
<h1>Citadela WebServer - %(title)s</h1>
<hr/>
<div class="content">
    %(content)s
</div>
<hr/>
<div class="footer">webserv v%(version)s by <a href="http://zamraky.cz">Gindar</a> %(timegen)s</div></body>