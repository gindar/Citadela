import os
from Citadela import bstruct
try:
    from md5 import md5
except:
    from hashlib import md5


class Storage:
    def __init__(self, path):
        self.path = path
        self.buffer = {}

    def getStrategyKey(self, name):
        return "strategy_%s" % md5(name).hexdigest()

    def _transName(self, name):
        name = "%s.py" % self.getStrategyKey(name)
        path = os.path.join(self.path, name)
        return path

    def saveBinary(self, name, data):
        data = bstruct.encode(data)
        self.saveFile(name, data)

    def saveFile(self, name, data):
        path = self._transName(name)
        fp = open(path, "wb")
        fp.write(data)
        fp.close()
        print "file saved %s" % name

    def addFilePart(self, name, data, part, part_count):
        if not name in self.buffer:
            self.buffer[name] = [0 for n in xrange(part_count)]
        self.buffer[name][part] = data
        print "file %s, part %s/%s, len %s" % (name, part + 1, part_count, len(data))

        # file is complete
        if not 0 in self.buffer[name]:
            data = ''.join(self.buffer[name])
            self.saveFile(name, data)
            del self.buffer[name]
            return True
        return False

    def getPath(self, name):
        path = self._transName(name)
        if os.path.exists(path):
            return path
        return None

    def getData(self, name):
        path = self._transName(name)
        if os.path.exists(path):
            fp = open(path, "rb")
            data = fp.read()
            fp.close()
            return data
        return None

    def getBinary(self, name):
        data = self.getData(name)
        if data:
            return bstruct.decode(data)
        else:
            return None
