### Helpers from GAZ


class GlobObject:
    __data__ = {}

    def getGlobal(self, key, default=None):
        return self.__data__.get(key, default)

    def setGlobal(self, key, value):
        self.__data__[key] = value


class IEvents:
    __listeners = {}

    def listen(self, event, callback):
        if not event in self.__listeners:
            self.__listeners[event] = []
        self.__listeners[event].append(callback)

    def event(self, event, *args):
        if not event in self.__listeners:
            return False

        for listener in self.__listeners[event]:
            listener(*args)


class Struct:
    def __init__(self, data=None):
        if data:
            if isinstance(data, dict):
                for key in data:
                    self.__dict__[key] = data[key]
            else:
                raise TypeError("Struct accepts only dict not %s" % type(data))

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value=None):
        self.__dict__[key] = value

    def __repr__(self):
        return "<Struct(%s)>" % repr(self.__dict__)

    def __contains__(self, key):
        return key in self.__dict__

    def copy(self):
        return Struct(self.__dict__.copy())

    def getDict(self):
        return self.__dict__
