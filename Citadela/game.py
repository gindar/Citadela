'''
    Citadela game - main game classes
    author: Josef Vanzura <gindar@zamraky.cz>
'''
import math
import random
from helpers import IEvents, Struct


'''
    Basic strategy parent class
'''


class BasicStrategy:
    def __init__(self, army_size=50):
        # link modules
        self.random = random
        self.math = math

        self.army_size = army_size

        # remaining soldiers
        self.remains = self.army_size

        # position from my citadel to enemy
        # MY -2 -1 0 1 2 ENEMY
        # position == 3 => WIN
        # position == -3 => LOST
        self.position = 0

    def _getSoldiers(self):
        v = self.getSoldiers()
        v = min(max(v, 1), self.remains)
        self.remains -= v
        return v

    def getSoldiers(self):
        return 1

    def warStart(self):
        self.remains = self.army_size
        self.position = 0

    def warEnd(self, result):
        # result:
        #   0 draw
        #   1 win
        #  -1 lost
        pass

    def battleEnd(self, result):
        # result:
        #   0 draw
        #   1 win
        #  -1 lost
        self.position += result


'''
    Import & check strategy
'''


class StrategyImporter(IEvents):
    strategy_list = []

    def loadStrategy(self, stratfname):
        # get strategy module info
        try:
            mod = __import__(stratfname, [], [], [], -1)
            clsobj = mod.Strategy
            strategy = Struct({
                "name":  clsobj.name,
                "version":  clsobj.version,
                "author":  clsobj.author,
                "path": stratfname,
                "clsobj": clsobj,
            })
            sinfo = "%s %s by %s" % (strategy.name, strategy.version, strategy.author)
            strategy.sinfo = sinfo

        except Exception, e:
            self.event("log-error", "loadstrategy", stratfname, e)
            return False

        self.strategy_list.append(strategy)
        self.event("log-info", "loadstrategy", stratfname, sinfo)
        return strategy


'''
    Play strategies
'''


class StrategyPlayer:
    def __init__(self, army_size=1000):
        self.army_size = army_size
        self.num_of_games = 1000
        self.output_stats = False

    def game(self, s1, s2):
        s1wins = 0
        s2wins = 0
        s1cls = s1.clsobj
        s2cls = s2.clsobj
        s1inst = s1cls(self.army_size)
        s2inst = s2cls(self.army_size)

        if self.output_stats:
            msg = "NAME s1 %s\n" % s1.name
            self.output_stats.write(msg)
            msg = "NAME s2 %s\n" % s2.name
            self.output_stats.write(msg)
            msg = "HELP s1.getSoldiers s2.getSoldiers s1.remains s2.remains\n"
            self.output_stats.write(msg)

        for n in xrange(self.num_of_games):
            s1result = 0
            s2result = 0
            s1inst.warStart()
            s2inst.warStart()
            result = self.war(s1inst, s2inst)
            if result == 1:
                s1wins += 1
                s1result = 1
                s2result = -1
            elif result == 2:
                s2wins += 1
                s2result = 1
                s1result = -1
            s1inst.warEnd(s1result)
            s2inst.warEnd(s2result)

        return (s1wins, s2wins)

    def war(self, s1inst, s2inst):
        # position: S1 -2 -1 0 1 2 S2
        info = Struct({
            "s1remains": self.army_size,
            "s2remains": self.army_size,
            "position": 0,
            "winner": 0
        })

        limit = 500
        while limit:
            result = self.battle(s1inst, s2inst, info)
            if result == 1:
                info.position += 1
            elif result == 2:
                info.position -= 1

            if info.position == 3:
                # player 1 wins
                info.winner = 1
                break

            if info.position == -3:
                # player 2 wins
                info.winner = 2
                break

            if info.s1remains == 0 or info.s2remains == 0:
                # player/s has no soldiers
                if info.winner == 0:
                    if info.position < 0:
                        info.winner = 2
                    if info.position > 0:
                        info.winner = 1
                break

            limit -= 1
            if limit == 0:
                info.winner = 0
                break

        if self.output_stats:
            msg = "WIN %s\n" % info.winner
            self.output_stats.write(msg)

        return info.winner

    def battle(self, s1i, s2i, info):
        s1n = s1i._getSoldiers()
        s2n = s2i._getSoldiers()

        # clamp values to range(1, remains)
        s1n = min(max(s1n, 1), info.s1remains)
        s2n = min(max(s2n, 1), info.s2remains)

        # lower remains counters
        info.s1remains -= s1n
        info.s2remains -= s2n

        if self.output_stats:
            msg = "N %s %s %s %s\n" % (s1n, s2n, info.s1remains, info.s2remains)
            self.output_stats.write(msg)

        if s1n < s2n:
            # player 2 win
            s1i.battleEnd(-1)
            s2i.battleEnd(1)
            return 2
        elif s1n > s2n:
            # player 1 win
            s1i.battleEnd(1)
            s2i.battleEnd(-1)
            return 1
        else:
            # plichta
            s1i.battleEnd(0)
            s2i.battleEnd(0)
            return 0
