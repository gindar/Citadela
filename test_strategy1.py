from Citadela.game import BasicStrategy


class Strategy(BasicStrategy):
    name = "Strategy1"
    author = "gindar"
    version = "1.0"

    def __init__(self, army_size=50):
        BasicStrategy.__init__(self, army_size)

    def getSoldiers(self):
        if self.remains < 3:
            return 1
        if self.position <= 1:
            return self.remains / 4

        if self.remains < self.army_size / 3:
            return self.remains / 2

        return self.random.randint(self.remains / 5, self.remains / 3)
