import socket
import bstruct
import thread
import time
from events import IEvents


class Client(IEvents):
    def __init__(self):
        self.server_addr = ""
        self.server_port = 80
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listen("client.send", self.send)
        self.listen("client.quit", self.quit)

    def connect(self, addr, port):
        self.server_addr = addr
        self.server_port = port
        self.sock.connect((addr, port))

    def recv(self):
        data = self.sock.recv(4096)
        if data:
            data = bstruct.decode(data)
            return data
        return None

    def send(self, data):
        data = bstruct.encode(data)
        self.sock.sendall(data)

    def sendFile(self, fname):
        file_part_size = 2048
        fp = open(fname, "rb")
        data = fp.read()
        fp.close()

        parts = []
        while len(data) > file_part_size:
            d = data[:file_part_size]
            data = data[file_part_size:]
            parts.append(d)
        if len(data) > 0:
            parts.append(data)

        index = 0
        for p in parts:
            self.send({"cmd": "file", "part": index,
                "parts": len(parts), "data": p
                })
            print "upload %s/%s" % (index+1, len(parts))
            
            if index < len(parts)-1:
                d = self.recv()
                if d["cmd"] != "fileok":
                    return False
            index += 1



    def quit(self):
        self.sock.close()
        self.event("client.closed")

