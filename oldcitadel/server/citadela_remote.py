import os
import json
import argparse
try:
    from md5 import md5
except:
    from hashlib import md5

import client

argparser = argparse.ArgumentParser(description='Server remote.')
argparser.add_argument('command', metavar='command', type=str, help='command')
argparser.add_argument('--host', default="localhost", help='hostname')
argparser.add_argument('--port', default=8207, type=int, help='port')
args = argparser.parse_args()

passwd = raw_input("Password:")
passwd = md5(passwd).hexdigest()

cli = client.Client()
cli.connect(args.host, args.port)
if args.command == "kill":
    cli.send({"cmd": "kill", "passwd": passwd})
    data = cli.recv()
    if data and "cmd" in data:
        if data["cmd"] == "message":
            print "Server: %s" % data["data"]

if args.command == "upload":
    cli.sendFile("bstruct.py")
    data = cli.recv()
    if data and "cmd" in data:
        if data["cmd"] == "message":
            print "Server: %s" % data["data"]
