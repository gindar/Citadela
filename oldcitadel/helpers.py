### Helpers from GAZ
### Josef Vanzura <gindar@zamraky.cz>


class Struct:
    def __init__(self, data=None):
        if data:
            if isinstance(data, dict):
                for key in data:
                    self.__dict__[key] = data[key]
            else:
                raise TypeError("Struct accepts only dict not %s" % type(data))

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value=None):
        self.__dict__[key] = value

    def __repr__(self):
        return "<Struct(%s)>" % repr(self.__dict__)

    def __contains__(self, key):
        return key in self.__dict__

    def copy(self):
        return Struct(self.__dict__.copy())

    def getDict(self):
        return self.__dict__
