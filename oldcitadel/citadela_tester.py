import statvis
from game import CitadelaGame

import random

def strategy1(remains, army_size, distance):
    q = 2 + random.random()
    return remains / q


def strategy2(remains, army_size, distance):
    return remains / 3.5

game = CitadelaGame()
game.games_count = 1000
game.collect_stats = True

game.play(strategy1, strategy2)

results = game.getResults()

statvis.printGameResults(results)
